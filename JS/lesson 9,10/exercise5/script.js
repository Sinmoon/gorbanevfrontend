let btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear");

//  __________BUTTONS____________
btn.addEventListener("click", function () {
    allObjInConsole(personArr);
    youngest (personArr);
});
clrBtn.addEventListener("click", function () {jsConsole.clearConsole()})

function youngest (arr) {
    let buff = arr[0];
    for (let i = 1; i<arr.length; i++) {
        arr[i].age < arr[i-1].age? buff = arr[i]: null;
    }
    jsConsole.writeLine("The youngest Person is:");
    jsConsole.writeLine(`${buff.firstName} ${buff.lastName}- ${buff.age}`)
    return buff
}

function allObjInConsole (arr) {
    jsConsole.writeLine("All persons:");
    for (let i =0; i<arr.length; i++) {
        for (key in arr[i]) {
            jsConsole.writeLine(`${key}: ${arr[i][key]}`)
        }
        jsConsole.writeLine("___________________________________________")
    }
}

let personArr = [
    { firstName : "Gosho", lastName: "Petrov", age: 32 },
    { firstName : "Bay", lastName: "Ivanov", age: 81 },
    { firstName : "Ivan", lastName: "Petrov", age: 12 },
    { firstName : "Chill", lastName: "Flexer", age: 7 }
]