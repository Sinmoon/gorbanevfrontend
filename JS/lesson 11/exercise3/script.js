let text = document.getElementById("text"),
    btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear"),
    exmplBtn = document.getElementById("example");

clrBtn.addEventListener("click", () => jsConsole.clearConsole());

exmplBtn.addEventListener("click", () => text.value = `<p>Please visit <a href="http://academy.telerik.com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>`);

btn.addEventListener("click", () =>htmlValue(text.value))

function htmlValue (text) {
    let regSite = /(http[s]?:\/\/[^\"]*)|(www[^\"]*)/g,
        regA = /(\<[a][^\>]*\>)/g;
    for (let i =  text.match(regA).length; i>0; i--) {
        text = text.replace(text.match(regA)[i-1], `[URL=${text.match(regSite)[i-1]}]`);
        text = text.replace(/(\<[\/][a][^\>]*\>)/, "[/URL]");
    }
    jsConsole.writeLine(text);
}