let numTrash = document.getElementById("numTrash"),
    valueTrash = document.getElementById("valueTrash"),
    basket = document.getElementById("basket"),
    container = document.getElementById("container"),
    btnCreateTrash = document.getElementById("btnCreateTrash");

    let currentDroppable = null;


 numTrash.addEventListener("change", ()=> {
     valueTrash.textContent = `Number of trash: ${numTrash.value}`;
 })   

btnCreateTrash.addEventListener("click", () => {
    if (document.getElementsByClassName("trash").length !== 0) {
        let trashCollection = document.getElementsByClassName("trash")
        for (let i = 0; i < trashCollection.length;) {
            trashCollection[i].remove()
        }
    }
    for (let i = 0; i<numTrash.value; i++) {
        createTrash()
    }
})


function createTrash () {
    let img = document.createElement("img");
    img.classList = ["trash"]
    img.src = "./img/trash.png";
    img.style.width = "75px"

    img.style.position = 'absolute';
    img.style.zIndex = 1000;
    let x = Math.random()*container.clientWidth;
    let y = Math.random()*container.clientHeight;
    if (x < 240 && y < 320) {
        x += 240;
        y += 320;
    }
    img.style.left = x+"px";
    img.style.top = y+"px";
    container.append(img);

    img.onmousedown = function(e) {
        function moveAt(pageX, pageY) {
            img.style.left = pageX - img.offsetWidth/2 + "px";
            img.style.top = pageY - img.offsetHeight/2 -320 + "px";
        }

        function mouseMove(e) {
            moveAt(e.pageX, e.pageY);
            
            img.hidden = true;
            let elemBelow = document.elementFromPoint(e.clientX, e.clientY);
            img.hidden = false;

            if (!elemBelow) return;

            let droppableBelow = elemBelow.closest(".droppable");
            if (currentDroppable != droppableBelow) {
                if (currentDroppable) {
                    leaveDroppable(currentDroppable);
                }
                currentDroppable = droppableBelow;
                if (currentDroppable) {
                    enterDroppable(currentDroppable);
                }
            }
        }

        document.addEventListener("mousemove", mouseMove);

        img.onmouseup = function() {
            document.removeEventListener("mousemove", mouseMove);
            img.onmouseup = null;
            if (currentDroppable.id == "basket") {
                img.remove();
                leaveDroppable(currentDroppable);
            }
        };
    }

    function enterDroppable(elem) {
        elem.src = "./img/open.jpg"
    }
    function leaveDroppable(elem) {
        elem.src = "./img/close.jpg"
    }
    img.ondragstart = function() {
        return false;
    }
}
