let arr = document.getElementById("array"),
    btn = document.getElementById("btn"),
    massive= [],
    bufArray =[],
    buf;

btn.addEventListener( "click", function () {
    maxBuf = 1;
    buf=1;
    bufArray = [];
    jsConsole.clearConsole();
    massive = arr.value.split(",");
    jsConsole.writeLine("The input sequence is:")
    jsConsole.writeLine(massive.join(", "));
    for (let i = 0; i<massive.length - 1; i++) {
        if (massive[i] === massive[i+1]) {
            buf++
            if (buf>maxBuf) {
                maxBuf = buf;
                bufArray = [];
                for (let j =0; j<maxBuf; j++) {
                    bufArray.push(massive[i]);
                }
            }
        } else {
            buf = 1;
        }
    }
    jsConsole.writeLine("");
    jsConsole.writeLine("The maximum sequence is:")
    jsConsole.writeLine(bufArray.join(", "));
});




