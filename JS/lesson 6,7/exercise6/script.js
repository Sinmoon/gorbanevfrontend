let text = document.getElementById("text"),
    btn = document.getElementById("btn");

btn.addEventListener( "click", startFunc);

function startFunc () {
    checkString(text)
}

function checkString (text) {
    let textVal = text.value
    jsConsole.writeLine(`${textVal.length < 20?textVal
        :textVal.slice(0,20)+"..."}`)
}