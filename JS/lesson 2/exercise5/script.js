let arr = document.getElementById("array"),
    btn = document.getElementById("btn"),
    massive= [],
    buf;

btn.addEventListener( "click", function () {
    massive = arr.value.split(",");
    jsConsole.writeLine("The input sequence is:");
    jsConsole.writeLine(massive.join(", "));
    for (let i = massive.length - 1; i > 0; i--) {
        for (let j = 0; j < i; j++) {
            if (Number(massive[j]) > Number(massive[j + 1])) {
                let buf = massive[j];
                massive[j] = massive[j + 1];
                massive[j + 1] = buf;
            }
        }
    }
    jsConsole.writeLine("");
    jsConsole.writeLine("The sorted array is:")
    jsConsole.writeLine(massive.join(", "));
});




