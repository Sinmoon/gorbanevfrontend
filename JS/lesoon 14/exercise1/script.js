let btn1 = document.getElementById("btn1"),
    btn2 = document.getElementById("btn2");

btn1.addEventListener("click", () => getElement("get"));
btn2.addEventListener("click", () => getElement("query"))

function getElement (word) {
    let coll = word === "get"?
        document.getElementsByTagName("div")
        :document.getElementsByTagName("div");
    for (let i = 0; i< coll.length; i++) {
        if (coll[i].id !== "console" && coll[i].children.length !==0) {
            for (let j = 0; j <coll[i].children.length; j++) {
                coll[i].children[j].style.border = word === "get"?
                `3px solid red`
                :`3px solid #0051ff`;
            }
        }
    }
}
