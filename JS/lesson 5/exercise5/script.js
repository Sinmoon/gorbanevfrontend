let text = document.getElementById("text"),
    btn = document.getElementById("btn");

function changeSpace(text) {
    let textArray = text.value.split("");
    textArray.forEach( (item, i) => {
        textArray[i] = item === " "? "&nbsp": item;
    })
    jsConsole.writeLine(`${textArray.join("").split(",")}`)
}

function startFunc () {
    changeSpace(text)
}

btn.addEventListener( "click", startFunc);