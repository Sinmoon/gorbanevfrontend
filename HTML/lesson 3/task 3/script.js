import mock from "./mock-data.js"

//  -------Изменение данных в таблице---------
function changeInfo(e) {
    let tbodyTr = document.getElementsByClassName("tbodyTr");
    let num = tbodyTr.length*(e.target.value-1);

    for (let i= 0; i< tbodyTr.length; i++) {
        for (let j=0; j < tbodyTr[i].children.length-1; j++) {
            tbodyTr[i].children[j].textContent = mock[i+num][j]
        }
    }
}

let prev = document.getElementById("prev");
let next = document.getElementById("next");
let first = document.getElementById("first");
let last = document.getElementById("last");

prev.addEventListener("click", prevPage);
next.addEventListener("click", nextPage);
first.addEventListener("click", firstPage);
last.addEventListener("click", lastPage);

function prevPage() {
    let tbodyTr = document.getElementsByClassName("tbodyTr");
    let firstElem = Number(document.getElementsByClassName("tbodyTr")[0]
    .children[0].textContent)
    if (firstElem >1) {
        for (let i= 0; i< tbodyTr.length; i++) {
            for (let j=0; j < tbodyTr[i].children.length-1; j++) {
                tbodyTr[i].children[j].textContent = mock[firstElem-tbodyTr.length-1+i][j]
            }
        }
    } 
}

function nextPage() {
    let tbodyTr = document.getElementsByClassName("tbodyTr");
    let firstElem = Number(document.getElementsByClassName("tbodyTr")[0]
    .children[0].textContent)
    let lastElem = Number(document.getElementsByClassName("tbodyTr")[tbodyTr.length-1]
    .children[0].textContent)
    if (lastElem < 30) {
        for (let i= 0; i< tbodyTr.length; i++) {
            for (let j=0; j < tbodyTr[i].children.length-1; j++) {
                tbodyTr[i].children[j].textContent = mock[firstElem+tbodyTr.length-1+i][j]
            }
        }
    } 
}

function firstPage() {
    let tbodyTr = document.getElementsByClassName("tbodyTr");
    for (let i= 0; i< tbodyTr.length; i++) {
        for (let j=0; j < tbodyTr[i].children.length-1; j++) {
            tbodyTr[i].children[j].textContent = mock[i][j]
        }
    }
}

function lastPage() {
    let tbodyTr = document.getElementsByClassName("tbodyTr");
    let num = mock.length-tbodyTr.length;
    for (let i= 0; i< tbodyTr.length; i++) {
        for (let j=0; j < tbodyTr[i].children.length-1; j++) {
            tbodyTr[i].children[j].textContent = mock[num+i][j]
        }
    }
}

// --------------Таблица-----------
function generateRow () {
    document.getElementById("tbody")?document.getElementById("tbody").remove() : null;
    
    if (document.getElementsByClassName("inpBtn")) {
        let btn = document.getElementsByClassName("inpBtn")
        for (let i= 0; i< btn.length; i) {
            btn[i].remove()
        }
    }

    let tabel = document.getElementById("table");
    let tbody  = document.createElement("tbody");
    tbody.id = "tbody"


    let num =document.getElementById("number");
    num.addEventListener("change", generateRow)

    // Генерация требуемого количества элементов
    for (let i=0; i<Number(num.value); i++) {
        let tr = document.createElement('tr');
        tr.className="tbodyTr"
        for (let j = 0; j < mock[i].length;  j++) {
            let td = document.createElement('td');
            td.className="td";
            td.textContent=mock[i][j]
            tr.append(td);
        }
        let input = document.createElement("input")
        let td = document.createElement('td');
        input.type="checkbox"
        td.append(input)
        tr.append(td);
        tbody.append(tr);
    }
    tabel.append(tbody)

    // Генерация кнопок
    let next = document.getElementById("next")
    let pageNum = Math.ceil(mock.length/num.value)
    for (let i = 0; i<pageNum; i++) {
        let inpBtn =  document.createElement("input")
        inpBtn.type = "button";
        inpBtn.className="inpBtn";
        inpBtn.value=i+1;
        inpBtn.addEventListener("click", changeInfo)

        
        next.before(inpBtn)
    }
}
generateRow()




