let inputs = document.getElementsByTagName("input")
for (let i = 0; i<inputs.length; i++) {
    inputs[i].addEventListener("input", changeValue)
}
function changeValue(e) {
    let span =  e.target.parentElement.parentElement.children[0]
    span.textContent = e.target.value
}
