let val = document.getElementById("val"),
    btn = document.getElementById("btn");

btn.addEventListener( "click", startFunc);


function startFunc() {
    fib(Number(val.value));
}

let fib = function (n) {
    let a = 1;
    let b =1;
    for (let i = 3; i <= n; i++) {
        let c = a+b;
        a = b;
        b = c;
    }
    jsConsole.writeLine(`The ${n}th Fibonacci  number is ${b}`)
}



