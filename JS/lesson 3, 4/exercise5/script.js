let str = document.getElementById("array"),
    value = document.getElementById("word"),
    btn = document.getElementById("btn"),
    testBtn = document.getElementById("test");

function findNum(str, value) {
    let array = str.value.split(","),
        num = value.value,
        count = 0;
    if (!num) {
        jsConsole.writeLine(`Enter some number`);
    } else {
        array.forEach( item => {
            item == num ? count++ : count;
        })
        jsConsole.writeLine(`Test array: [${array}]`);
        jsConsole.writeLine(`Test number "${num}" occurs ${count} times`);
    }
}

function testFunc () {
    let count = 0,
        array = [],
        num = Math.round(Math.random()*20);

    for (let i = 0; i<10; i++){
        array.push(Math.round(Math.random()*20));
    }
    array.forEach( item => {
        item == num ? count++ : count;
    })
    jsConsole.writeLine(`Array: [${array}]`);
    jsConsole.writeLine(`The number "${num}" occurs ${count} times`);
}

function startFunc () {
    findNum(str, value);
}

btn.addEventListener( "click", startFunc);
testBtn.addEventListener("click", testFunc)