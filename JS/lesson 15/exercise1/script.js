let body = document.getElementsByTagName("body")[0];
function start () {
  setTimeout (()=>{
    drawMan();
    drawBicycle();
    drawHouse();

  }, 10);
}

function drawHouse () {
  let canvas = document.getElementById('house');
  canvas.width = "600";
  canvas.height = "600";
  canvas.style.border = "1px solid black";
  if (canvas.getContext){
    let ctx = canvas.getContext('2d');

    // room
    ctx.beginPath();
    ctx.save()
    ctx.lineWidth = 3;
    ctx.rect(75, 240, 450, 350);
    ctx.fillStyle = "rgb(151, 91, 91)";
    ctx.strokeStyle = "black";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();

    // roof 
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.moveTo(74, 240);
    ctx.lineTo(300, 50);
    ctx.lineTo(524, 240);
    ctx.lineTo(75, 240);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();

    // chimney
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.rect(400, 100, 50, 100);
    ctx.fill();
    ctx.closePath();

    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.moveTo(450, 200);
    ctx.lineTo(450, 100);
    ctx.ellipse(425,100, 25, 10, 0, 0, Math.PI*2, false);
    ctx.moveTo(400, 100)
    ctx.lineTo(400, 200);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();

    // windows
    ctx.beginPath();
    ctx.restore()

    ctx.fillRect(100, 270, 80,50);
    ctx.fillRect(185, 270, 80,50);
    ctx.fillRect(100, 325, 80,50);
    ctx.fillRect(185, 325, 80,50);

    ctx.fillRect(335, 270, 80,50);
    ctx.fillRect(420, 270, 80,50);
    ctx.fillRect(335, 325, 80,50);
    ctx.fillRect(420, 325, 80,50);

    ctx.fillRect(335, 425, 80,50);
    ctx.fillRect(420, 425, 80,50);
    ctx.fillRect(335, 480, 80,50);
    ctx.fillRect(420, 480, 80,50);

    ctx.closePath();

    // door
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.moveTo(110, 590);
    ctx.lineTo(110, 450);

    ctx.moveTo(175, 590);
    ctx.lineTo(175, 417);

    ctx.moveTo(240, 590);
    ctx.lineTo(240, 450);

    ctx.moveTo(168, 550);
    ctx.arc(160, 550, 8, 0, Math.PI*2);

    ctx.moveTo(198, 550);
    ctx.arc(190, 550, 8, 0, Math.PI*2);

    ctx.moveTo(240, 590);
    ctx.scale(1, 0.5)
    ctx.arc(175, 900, 65, 0, Math.PI, true)
    ctx.stroke();
    ctx.restore()
    ctx.closePath();
  }
}

function drawBicycle(){
  let canvas = document.getElementById('bicycle');
  canvas.width = "600";
  canvas.height = "400";
  canvas.style.border = "1px solid black";
  if (canvas.getContext){
    let ctx = canvas.getContext('2d');

    // wheels
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.arc(140, 250, 90, 0, Math.PI*2, false);
    ctx.moveTo(540, 250)
    ctx.arc(450, 250, 90, 0, Math.PI*2, false);
    ctx.fillStyle = "rgb(144, 202, 215)";
    ctx.strokeStyle = "rgb(51, 125, 143)";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
    
    // details 
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.moveTo(140, 240)
    ctx.lineTo(240, 140);
    ctx.lineTo(430, 140);
    ctx.lineTo(280, 230);
    ctx.lineTo(140, 240);

    ctx.moveTo(280, 230);
    ctx.lineTo(220, 100);
    ctx.moveTo(180, 100);
    ctx.lineTo(260, 100);

    ctx.moveTo(450, 250);
    ctx.lineTo(425, 80);
    ctx.lineTo(380,100)
    ctx.moveTo(425, 80);
    ctx.lineTo(460, 40)

    ctx.moveTo(305, 230);
    ctx.arc(280, 230, 25, 0, Math.PI*2, false);
    ctx.moveTo(260, 215);
    ctx.lineTo(240, 190);
    ctx.moveTo(295, 250);
    ctx.lineTo(310, 270);

    ctx.stroke();
    ctx.closePath();
    }
}


function drawMan(){
  let canvas = document.getElementById('man');
  canvas.style.width = "200";
  canvas.height = "400";
  canvas.style.border = "1px solid black";
  if (canvas.getContext){
    let ctx = canvas.getContext('2d');

    // Head
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.arc(140, 200, 90, 0, Math.PI*2, false);
    ctx.fillStyle = "rgb(144, 202, 215)";
    ctx.strokeStyle = "rgb(29, 79, 89)";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();

    // Face
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.ellipse(80, 180, 20, 10, 0, 0, Math.PI*2, false);
    ctx.moveTo(180, 180);
    ctx.ellipse(160, 180, 20, 10, 0, 0, Math.PI*2, false);
    ctx.moveTo(160, 270)
    ctx.ellipse(120, 260, 40, 20, Math.PI/10, 0, Math.PI*2, false)
    ctx.moveTo(130, 180);
    ctx.lineTo(110, 220)
    ctx.lineTo(130,220)
    ctx.strokeStyle = "rgb(29, 79, 89)";
    ctx.stroke();
    ctx.closePath();

    // Eyes
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.ellipse(75, 180, 7, 10, 0, 0,  Math.PI*2, false);
    ctx.ellipse(155, 180, 7, 10, 0, 0,  Math.PI*2, false);
    ctx.fillStyle="rgb(28, 78, 87)";
    ctx.fill();
    ctx.closePath();

    // Cylinder
    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.save();
    ctx.scale(1, 0.25);
    ctx.arc(140, 550, 100, 0, Math.PI*2, false)
    ctx.restore();
    ctx.fillStyle = "rgb(57, 102, 147)";
    ctx.strokeStyle = "black";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();

    ctx.beginPath();
    ctx.lineWidth = 3;
    ctx.scale(1, 0.5)
    ctx.arc(140, 100, 50, 0, Math.PI*2, false);
    ctx.arc(140, 250, 50, 0, Math.PI, false);
    ctx.lineTo(90, 100);
    ctx.fillStyle = "rgb(57, 102, 147)";
    ctx.strokeStyle = "black";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
    
    }
}

start()