let color = document.getElementById("color"),
    btn = document.getElementById("btn"),
    cons = document.getElementById("console");

btn.addEventListener("click", () => changeColor(color.value));

function changeColor (color) {
    cons.style.backgroundColor = color;
}