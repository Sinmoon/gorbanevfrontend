let a = document.getElementById("a"),
    b = document.getElementById("b"),
    sum = document.getElementById("s"),
    odd = document.getElementById("odd"),
    resBtn = document.getElementById("res");

resBtn.addEventListener("click", function () {
    let aVal = Number(a.value),
            bVal = Number(b.value),
            sumValue=0;
    odd.value="";
    if( a.value && b.value ) {
        // Sum from A to B
        if ( aVal<bVal ) {
            for (let i=0; i< bVal-aVal+1; i++) {
                // Sum from A to B
                sumValue += aVal+i;
                // odd
                (aVal+i) % 2 !== 0? odd.value+=`${aVal+i} `:null;
            }
            sum.value = sumValue
        } else {
            sum.value = "Enter B<A"
        }
    } else {
        sum.value = "Enter A, B"
    }
})