let text = document.getElementById("text"),
    value = document.getElementById("word"),
    reg = document.getElementById("reg"),
    btn = document.getElementById("btn");

function findWord(text, value, check=false) {
    let textArray = text.value.split(" "),
        word = value.value,
        count = 0;
    if (check) {
        textArray.forEach( (item, i) => {
            textArray[i] = item.toLowerCase();
        });
        textArray.forEach( item => {
            item == word.toLowerCase() ? count++ : count;
        })
    } else {
        textArray.forEach( item => {
            item == word ? count++ : count;
        })
    }
    jsConsole.writeLine(`The word "${word}" occurs ${count} times`)
}

function checkReg () {
    reg.checked? findWord(text, value, reg.checked) : findWord(text, value)
}

btn.addEventListener( "click", checkReg);