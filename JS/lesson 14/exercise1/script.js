let btnAdd = document.getElementById("addItem"),
    btnRemove = document.getElementById("removeItem"),
    btnHide = document.getElementById("hideItem"),
    btnShow = document.getElementById("showItem"),
    item = document.getElementById("item"),
    ul = document.getElementById("list-item"),
    check = document.getElementsByClassName("setting")[0],
    clearInput = document.getElementsByClassName("setting")[1];

btnAdd.addEventListener("click", addItem);
btnRemove.addEventListener("click", removeItems);
btnHide.addEventListener("click", hideItems);
btnShow.addEventListener("click", showItems)

function addItem () {
    let li = document.createElement("li");
    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.name = "item";

    let newItem = item.value
    li.append(checkbox);
    li.append(newItem);
    
    ul.append(li)
    clearInput.checked? item.value = "": null;
}

function removeItems () {
    for (let i=0; i<ul.children.length; i++) {
        if (check.checked === false) {
            if (ul.children[i].children[0].checked === true ) {
                ul.children[i].remove();
                i--
            }
        } else {
            if (ul.children[i].children[0].checked === true
                && ul.children[i].style.display !== "none") {
                ul.children[i].remove()
                i--
            }
        }
    }
}

function hideItems () {
    for (let i=0; i<ul.children.length; i++) {
        if (ul.children[i].children[0].checked === true) {
            ul.children[i].style.display = "none"
        }
    }
}
function showItems () {
    for (let i=0; i<ul.children.length; i++) {
        if (ul.children[i].children[0].checked === true) {
            ul.children[i].style.display = "block"
        }
    }
}