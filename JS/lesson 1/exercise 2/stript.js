let x1 = document.getElementById("x1"),
    x2 = document.getElementById("x2"),
    x3 = document.getElementById("x3"),
    y = document.getElementById("y"),
    resBtn = document.getElementById("res");

resBtn.addEventListener("click", function () {
    if( x1.value && x2.value && x3.value) {
        y.value = (Number(x1.value)+Number(x2.value)+Number(x3.value))/3;
    } else {
        y.value = "Enter X1, X2, X3"
    }
})