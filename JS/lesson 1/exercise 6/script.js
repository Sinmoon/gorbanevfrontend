let a = document.getElementById("a"),
    b = document.getElementById("b"),
    rec = document.getElementById("rectangle"),
    resRec = document.getElementById("resRec");

resRec.addEventListener("click", function () {
    let aVal = Number(a.value),
            bVal = Number(b.value);
    rec.textContent="";
    if( a.value && b.value ) {
        for (let i = 0 ; i<aVal; i++ ) {
            for( let j = 0; j<bVal; j++) {
                rec.innerHTML += "*&nbsp&nbsp&nbsp";
            }
            rec.innerHTML += "<br>";
        }
    } else {
        rec.textContent = "Enter height, wigth"
    }
})

// Прямоуг треуг
let leg = document.getElementById("leg"),
    resRightTr = document.getElementById("resRightTr"),
    rightTr = document.getElementById("rightTr");
    
resRightTr.addEventListener("click", function () {
    let legVal = Number(leg.value);
    for (let i = 0 ; i<legVal; i++ ) {
        for( let j = 0; j<i+1; j++) {
            rightTr.innerHTML += "*&nbsp&nbsp&nbsp";
        }
        rightTr.innerHTML += "<br>";
    }
})
// Равносторонний треуг
let wEgTr = document.getElementById("wigthEgTr"),
    resEgTr = document.getElementById("resEgTr"),
    egTr = document.getElementById("egTr");
    
resEgTr.addEventListener("click", function () {
    let wEgTrVal = Number(wEgTr.value);
    egTr.textContent="";
    for (let i = 0 ; i<wEgTrVal; i++ ) {
        for (let k = 0; k<(3)*(wEgTrVal-i-1); k++) {
            egTr.innerHTML += "&nbsp"
        }
        for( let j = 0; j<i+1; j++) {         
            egTr.innerHTML += "*&nbsp&nbsp&nbsp&nbsp";
        }
        egTr.innerHTML += "<br>";
    }
})

// Ромб треуг
let wRhom = document.getElementById("wigthRhom"),
    resRhom = document.getElementById("resRhom"),
    rhom = document.getElementById("rhom");

resRhom.addEventListener("click", function () {
    let wRhomVal = Number(wRhom.value);
    rhom.textContent="";
    for (let i = 0 ; i<wRhomVal; i++ ) {
        for (let k = 0; k<(3)*(wRhomVal-i-1); k++) {
            rhom.innerHTML += "&nbsp"
        }
        for( let j = 0; j<i+1; j++) {     
            rhom.innerHTML += "*&nbsp&nbsp&nbsp&nbsp";
        }
        rhom.innerHTML += "<br>";
    }

    for (let i = 0 ; i<wRhomVal-1; i++ ) {
        for (let k = 0; k<(3)*(i+1); k++) {
            rhom.innerHTML += "&nbsp"
        }
        for( let j = 0; j<wRhomVal-i-1; j++) {     
            rhom.innerHTML += "*&nbsp&nbsp&nbsp&nbsp";
        }
        rhom.innerHTML += "<br>";
    }
})