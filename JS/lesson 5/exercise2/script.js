let value = document.getElementById("str"),
    btn = document.getElementById("btn");

function checkString (string) {
    let array = string.split("");
    let count1 = 0,
        count2 = 0,
        flag = false;
    for (let i = 0; i< array.length; i++) {
        if (count1 < count2) {
            break;
        }

        if (array[i] === "(") {
            ++count1;
            if (array[i+1] === ")") {
                break;
            } 
        }

        if (array[i] === ")") {
            if (array[i-1] !== "*" && array[i-1] !== "/" && array[i-1] !== "-" && array[i-1] !== "+") {
                ++count2
            } else {
                break;
            }
            
        }
        if (i === array.length-1 && count1 === count2 ) {
            flag = true;
        }
    };

    flag == true?
        jsConsole.writeLine("Valid expression!")
        : jsConsole.writeLine("Wrong expression!");
    
}
function execute () {
    let string = value.value;
    checkString(string);
}
btn.addEventListener( "click", execute);




