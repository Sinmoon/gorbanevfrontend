let divGet = document.getElementById("divGet"),
    btnGet = document.getElementById("btnGet"),
    divPost = document.getElementById("divPost"),
    btnPost = document.getElementById("btnPost");

btnGet.addEventListener("click", () => {
    getJSON().then( 
        (response) => divGet.innerHTML = response, 
        (error) => divGet.innerHTML = `${error.status} ${error.statusText}`
    );
})

btnPost.addEventListener("click", () => {
    if (!document.getElementById("form-container")) {
        createForm();
        document.getElementById("form-container").style.display ="block"
    } else {
        if (document.getElementById("form-container").style.display ==="block") {
            document.getElementById("form-container").style.display ="none"

            let form = document.getElementById("form-container");
            let formdata = new FormData(form);
            let data = {
                firstName: formdata.get("fName"),
                lastName: formdata.get("lName"),
                age: formdata.get("age")
            }

            postJSON(JSON.stringify(data)).then( 
                (response) => divPost.innerHTML = response, 
                (error) => divPost.innerHTML = `${error.status} ${error.statusText}`
            );
        } else {
            document.getElementsByName("fName")[0].value = "";
            document.getElementsByName("lName")[0].value = "";
            document.getElementsByName("age")[0].value = "";

            document.getElementById("form-container").style.display ="block"
        }
    };
})

function createForm () {
    let container = document.createElement("form");
        container.id = "form-container";
       container.innerHTML = `First name: <input type="text" name="fName"> 
       LastName: <input type="text" name="lName"> 
       Age: <input type="text" name="age">`;

    btnPost.before(container);
}

function getJSON() {
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.open("GET", "01.data.json");
        request.onload = function () {
            if(this.status >= 200 && this.status < 300) {
                resolve(request.response);
            } else {
                reject ({
                    status: this.status,
                    statusText:this.statusText
                })
            }
        };
        request.onerror = function() {
            reject ({
                status: this.status,
                statusText: this.statusText
            })
        }
        request.send();
    });
}

function postJSON(data) {
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();

        request.open("POST",  "submit.php");
        request.setRequestHeader("Conent-type", "application/json");

        request.onload = function () {
            if(this.status >= 200 && this.status < 300) {
                resolve(request.responseText);
            } else {
                reject ({
                    status: this.status,
                    statusText:this.statusText
                })
            }
        };
        request.onerror = function() {
            reject ({
                status: this.status,
                statusText: this.statusText
            })
        };
        request.send(data);
    });
}