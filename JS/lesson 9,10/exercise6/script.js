let clrBtn = document.getElementById("clear"),
    allBtn = document.getElementById("all"),
    fnameBtn = document.getElementById("fnameBtn"),
    lnameBtn = document.getElementById("lnameBtn"),
    ageBtn = document.getElementById("ageBtn");
//  __________BUTTONS____________
clrBtn.addEventListener("click", () => {jsConsole.clearConsole()});
allBtn.addEventListener("click", () => {allObjInConsole(persons)});

fnameBtn.addEventListener("click", function () {sortFunc(persons, "firstname")})
lnameBtn.addEventListener("click",function () {sortFunc(persons, "lastname")})
ageBtn.addEventListener("click", function () {sortFunc(persons, "age")})

function sortFunc (arr, word) {
    let arrayValue = [];
    for (let i=0; i<arr.length; i++) {
        arrayValue.some((item) => item === arr[i][word])?null : arrayValue.push(arr[i][word])
    }
    typeof arrayValue[0] === "string"?arrayValue.sort():arrayValue.sort((a, b) => a - b)
    jsConsole.writeLine(arrayValue)
}

function allObjInConsole (arr) {
    jsConsole.writeLine("");
    jsConsole.writeLine("All persons:");
    for (let i =0; i<arr.length; i++) {
        for (key in arr[i]) {
            jsConsole.writeLine(`${key}: ${arr[i][key]}`)
        }
        jsConsole.writeLine("___________________________________________")
    }
}

let persons = [
    { firstname: "Natalya", lastname: "Osipenko", age: 61 },
    { firstname: "Kristina", lastname: "Osipenko", age:23 },
    { firstname: "Artem", lastname: "Korhov", age: 25 },
    { firstname: "Artem", lastname: "Seredinskiy", age: 20 },
    { firstname: "Artem", lastname: "Artsiomenka", age: 20 },
    { firstname: "Sergey", lastname: "Osipenko", age: 20 },
    { firstname: "Vinni", lastname: "Puh", age: 15 }
];
