let btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear");

clrBtn.addEventListener("click", () => jsConsole.clearConsole());
btn.addEventListener("click", () =>stringFormat("The coordinates of the {0} are X:{1} and Y: {2}", "rectangle", 25, 36));

function stringFormat (string, ...spread) {
    let reg = /(\{\d\})/g;
    let stringMatch = string.match(reg);
    for (let i = 0; i<stringMatch.length; i++) {
        string = string.replace(stringMatch[i], spread[i]);
    }
    jsConsole.writeLine(string);
}