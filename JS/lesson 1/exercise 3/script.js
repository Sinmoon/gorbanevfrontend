let cylRadius = document.getElementById("r"),
    cylHeight = document.getElementById("h"),
    square = document.getElementById("s"),
    volume = document.getElementById("v"),
    resBtn = document.getElementById("res");

    resBtn.addEventListener("click", function () {
        if( cylRadius.value && cylHeight.value) {
            square.value = Math.PI.toFixed(2)* cylRadius.value * cylRadius.value;
            volume.value =(square.value*cylHeight.value).toFixed(2);
        } else {
            volume.value = "Enter R, h"
        }
    })