let text = document.getElementById("text"),
    btn = document.getElementById("btn");

btn.addEventListener( "click", startFunc);

function startFunc () {
    checkSpam(text)
}

function checkSpam (text) {
    let textArr = text.value.split(" ")
    for (let i = 0; i<textArr.length; i++) {
        if (textArr[i].toLowerCase().includes("spam") || textArr[i].toLowerCase().includes("sex")) {
            jsConsole.writeLine("true");
            return;
        }
    }
    jsConsole.writeLine("false");
}