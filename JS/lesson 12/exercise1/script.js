let text = document.getElementById("text"),
    btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear"),
    exmplBtn = document.getElementById("example");

clrBtn.addEventListener("click", () => jsConsole.clearConsole());
exmplBtn.addEventListener("click", () => text.value = "my email: myemail@hoo.kek, my friend's email: myfriend@fri.nd, my father's email: father@papa.com");
btn.addEventListener("click", () =>textValue(text.value))

function textValue (text) {
    let reg = /(?<=\s)([^\s]*\@[^\s]*\.[\w]*)/g;
    for (let i = 0; i<text.match(reg).length; i++) {
        jsConsole.writeLine(text.match(reg)[i]);
    }
}