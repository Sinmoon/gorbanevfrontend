let fname = document.getElementById("fname"),
    lname = document.getElementById("lname"),
    age = document.getElementById("age"),
    btn = document.getElementById("btn"),
    cbtn = document.getElementById("cbtn"),
    wbtn = document.getElementById("wbtn");


//  __________BUTTONS____________
btn.addEventListener("click", funcStart);

cbtn.addEventListener("click", () => {fname.value = "Ivan"; lname.value = "Chebypelli"; age.value = 21});

wbtn.addEventListener("click", () => {fname.value = "Iv"; lname.value = "Chel"; age.value = 151});
// __________________________________

function funcStart () {
    let man = new Person();
    man.age = age.value;
    man.fullname = `${fname.value} ${lname.value}`;
    man.introduce();
    console.log(man)
}

function Person () {
}

Person.prototype = {
    // ___________AGE___________
    set age (stringAge) {
        if (Number(stringAge) >=0 
        && Number(stringAge)<=150) {
            this._age = Number(stringAge);
        } else if (stringAge == "null") {
            this._age = 0;
        } else {
            jsConsole.writeLine("Incorrect age!")
        }
    },
    get age () {
        return this._age
    },
    // _________FULLNAME___________
    get fullname () {
        return this.fullname = `${this.firstname} ${this.lastname}`
   },
   set fullname (string) {
       let nameArr = string.split(" ");
       re=new RegExp('^[a-zA-Z]+$');
       if ( nameArr.length === 2
       && re.test(nameArr[0])
       && re.test(nameArr[1])
       && nameArr[0].length >= 3 
       && nameArr[0].length <= 20
       && nameArr[1].length >= 3 
       && nameArr[1].length <= 20) {
           this.firstname = nameArr[0];
           this.lastname = nameArr[1];
       } else {
           jsConsole.writeLine("Incorrect name!");
       }
   }
    
};

Person.prototype.introduce = function () {
    if (this.firstname && this.lastname) {
        jsConsole.writeLine(`Hello! My name is ${this.fullname} and I am ${this.age}-years-old`)
    }
}
