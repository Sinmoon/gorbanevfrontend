let text = document.getElementById("text"),
    btn = document.getElementById("btn"),
    test = document.getElementById("test")



btn.addEventListener( "click", startFunc);

test.addEventListener("click", () => {
    text.value = "<mixcase>It is mix text, <upcase>upper in mix, <lowcase>LOWER IN UPPER IN MIX, <mixcase>mix text,</mixcase> <upcase>up text,</upcase> LOW TEXT,</lowcase> up text,</upcase> mix end.</mixcase>"
})

function findTag(text) {  
    let textArray = text.value.split(" ");
    for (let i = 0; i < textArray.length; i++) {
        textArray[i].toLowerCase().includes("<upcase>")? change("up", i, textArray): textArray[i];
        textArray[i].toLowerCase().includes("<lowcase>")? change("low", i, textArray): textArray[i];
        textArray[i].toLowerCase().includes("<mixcase>")? change("mix", i, textArray): textArray[i];
    }

    jsConsole.writeLine(textArray.join(" "))
}

function change (func, index, arr) {
    let bufS = 0;
    let bufF = 0;
    switch (func) {
        // ______________UPPER__________________
        case "up" :
            for (let i = index; i<arr.length; i++) {
                if (arr[i].toLowerCase().includes("<upcase>")) {
                    ++bufS;
                }
                if (arr[i].toLowerCase().includes("</upcase>")) {
                    ++bufF;
                    if (bufS===bufF) {
                        arr[i] = upCase(arr[i]);
                        break;
                    }
                }
                arr[i] = upCase(arr[i]);
            }
            break;
        // ______________LOWER______________
        case "low" :
            for (let i = index; i<arr.length; i++) {
                if (arr[i].toLowerCase().includes("<lowcase>")) {
                    ++bufS;
                }
                if (arr[i].toLowerCase().includes("</lowcase>")) {
                    ++bufF;
                    if (bufS===bufF) {
                        arr[i] = lowCase(arr[i]);
                        break;
                    }
                }
                arr[i] = lowCase(arr[i]);
            }
            break;
        // _______________MIX______________
        case "mix" : 
            for (let i = index; i<arr.length; i++) {
                if (arr[i].toLowerCase().includes("<mixcase>")) {
                    ++bufS;
                }
                if (arr[i].toLowerCase().includes("</mixcase>")) {
                    ++bufF;
                    if (bufS===bufF) {
                        arr[i]=mixCase(arr[i])
                    break;
                    }           
                }
                console.log(arr[i])
                arr[i] = mixCase(arr[i])        
            }
        default: 
            break;
    }
}

function upCase (word) {
    return word.toUpperCase();
}

function lowCase (word) {
    return word.toLowerCase();
}

function mixCase(word) {
    for (let i = 0; i<word.length; i++) {
        let wordArr = word.split("")  
        for (let j = 0; j<wordArr.length; j++) {
            if (j % 2 !==0) {
                wordArr[j] = wordArr[j].toUpperCase();
            } else {
                wordArr[j] = wordArr[j].toLowerCase();
            }
        }
        return wordArr.join("");
    }
}

function startFunc () {
    findTag(text)
}