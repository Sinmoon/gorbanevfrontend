let num = document.getElementById("num"),
    trueNum = document.getElementById("trueNum"),
    vector = document.getElementById("vector"),
    res = document.getElementById("res");

res.addEventListener("click", function () {
    let numValue = Number(num.value);
    let count =0;
    let arr = [];
    vector.value="";
    trueNum.value="";
    for (let i=0; i<numValue; i++) {
        let number = Math.round((Math.random()*20)*(Math.random()<0.5?-1:1))
        arr.push(number);
        vector.value += `${number},`;
        Math.abs(number)>7?count++:null;
    }
    vector.value = vector.value.slice(0, vector.value.length -1)
    trueNum.value = count;
})