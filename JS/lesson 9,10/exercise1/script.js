let l1p1x = document.getElementById("L1p1x"),
    l1p1y = document.getElementById("L1p1y"),
    l1p2x = document.getElementById("L1p2x"),
    l1p2y = document.getElementById("L1p2y"),

    l2p1x = document.getElementById("L2p1x"),
    l2p1y = document.getElementById("L2p1y"),
    l2p2x = document.getElementById("L2p2x"),
    l2p2y = document.getElementById("L2p2y"),

    l3p1x = document.getElementById("L3p1x"),
    l3p1y = document.getElementById("L3p1y"),
    l3p2x = document.getElementById("L3p2x"),
    l3p2y = document.getElementById("L3p2y"),

    btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear"),
    corBtn = document.getElementById("correct"),
    wrongBtn = document.getElementById("wrong")

//  __________BUTTONS____________
btn.addEventListener("click", funcStart);
clrBtn.addEventListener("click", function () {jsConsole.clearConsole()})
corBtn.addEventListener("click", function () {
    // line 1
    l1p1x.value = 0;
    l1p1y.value =0;
    l1p2x.value = 2;
    l1p2y.value =2;
    // line 2
    l2p1x.value = 0;
    l2p1y.value =0;
    l2p2x.value = 3;
    l2p2y.value =3;
    // line 3
    l3p1x.value = 0;
    l3p1y.value =0;
    l3p2x.value = 4;
    l3p2y.value =4;
})
wrongBtn.addEventListener("click", function () {
    // line 1
    l1p1x.value = 0;
    l1p1y.value =0;
    l1p2x.value = 8;
    l1p2y.value =16;
    // line 2
    l2p1x.value = 0;
    l2p1y.value =0;
    l2p2x.value = 3;
    l2p2y.value =3;
    // line 3
    l3p1x.value = 0;
    l3p1y.value =0;
    l3p2x.value = 8;
    l3p2y.value =50;
})


function funcStart () {
    let tri = new Triangle ();
    let arrLine1 = [Number(l1p1x.value), Number(l1p1y.value), Number(l1p2x.value), Number(l1p2y.value)]
    let arrLine2 = [Number(l2p1x.value), Number(l2p1y.value), Number(l2p2x.value), Number(l2p2y.value)];
    let arrLine3 = [Number(l3p1x.value), Number(l3p1y.value), Number(l3p2x.value), Number(l3p2y.value)]
    //  line 1
    tri.createLine(Object.keys(tri).length+1);

    tri.createPoint(arrLine1[0], arrLine1[1], Object.keys(tri).length, Object.keys(tri.line1).length+1 );

    tri.createPoint(arrLine1[2], arrLine1[3], Object.keys(tri).length, Object.keys(tri.line1).length+1 );

    tri.calcLength(1);

    // line 2
    tri.createLine(Object.keys(tri).length+1);

    tri.createPoint(arrLine2[0], arrLine2[1], Object.keys(tri).length, Object.keys(tri.line2).length+1 );

    tri.createPoint(arrLine2[2], arrLine2[3], Object.keys(tri).length, Object.keys(tri.line2).length+1 );

    tri.calcLength(2);

    // line 3 
    tri.createLine(Object.keys(tri).length+1);

    tri.createPoint(arrLine3[0], arrLine3[1], Object.keys(tri).length, Object.keys(tri.line3).length+1 );

    tri.createPoint(arrLine3[2], arrLine3[3], Object.keys(tri).length, Object.keys(tri.line3).length+1 );

    tri.calcLength(3);

    tri.canBeTriangle()
}

function Triangle () {
}

Triangle.prototype.createPoint = function (x, y, numLine, numPoint) {
    this[`line${numLine}`][`point${numPoint}`] = [x, y];
    jsConsole.writeLine(`&nbsp&nbspPoint #${numLine}: (${x}, ${y})`);
}

Triangle.prototype.createLine = function (numLine) {
    this[`line${numLine}`] = {};
    jsConsole.writeLine(`Line #${numLine}:`)
}

Triangle.prototype.calcLength = function (numLine) {
    let res = lengthL(this[`line${numLine}`].point1[0],this[`line${numLine}`].point1[1], this[`line${numLine}`].point2[0],this[`line${numLine}`].point2[1])
    this[`line${numLine}`].length = res;

    jsConsole.writeLine(`&nbsp&nbspThe length: ${res}`);
    jsConsole.writeLine(" ")
}

function lengthL (x1, y1, x2, y2) {
    let lengthX = Math.abs(x1-x2);
    let lengthY = Math.abs(y1-y2);
   return Math.sqrt(Math.pow(lengthX, 2) + Math.pow(lengthY, 2)).toFixed(2);
}

Triangle.prototype.canBeTriangle = function () {
    if (Number(this.line1.length) + Number(this.line2.length) > Number(this.line3.length)
        && Number(this.line2.length) + Number(this.line3.length) > Number(this.line1.length)
        && Number(this.line1.length) + Number(this.line3.length) > Number(this.line2.length)
        ) {
            jsConsole.writeLine("The lines CAN create triangle!");
            jsConsole.writeLine("______________________________");
            jsConsole.writeLine("");
        } else {
            jsConsole.writeLine("The lines CAN'T create triangle!");
            jsConsole.writeLine("________________________________");
            jsConsole.writeLine("");
        }
        
}