let btn1 = document.getElementById("btn1"),
    btn2 = document.getElementById("btn2"),
    btn3 = document.getElementById("btn3"),

    divNum = document.getElementById("divNum"),
    divNumValue = document.getElementById("divNumValue"),
    
    divSpeed = document.getElementById("divSpeed"),
    divSpeedValue = document.getElementById("divSpeedValue"),

    cons = document.getElementById("console"),
    intervalID=null;

divNum.addEventListener("change", ()=> {
    divNumValue.textContent = `Number: ${divNum.value}`;
})
divSpeed.addEventListener("change", ()=> {
    divSpeedValue.textContent = `Speed: ${divSpeed.value}`;
    animateCSS();
    if (Boolean(intervalID)) {
        clearInterval(intervalID);
        intervalID = null;
    }
})

btn1.addEventListener("click", () => {
    createDivs();
    animateCSS();
    if (Boolean(intervalID)) {
        clearInterval(intervalID);
        intervalID = null;
    }
});
btn2.addEventListener("click", () => {
    animateCSS(true);
    if (Boolean(intervalID)) {
        clearInterval(intervalID);
        intervalID = null;
    }
});
btn3.addEventListener("click", () => {
    animateCSS();
    animateSetInterval();
})

function createDivs () {
    if (document.getElementById("div-container")) {
        document.getElementById("div-container").remove()
    }
    let div = document.createElement("div")
    div.id = "div-container";
    cons.children[0].append(div);
    let num = Number(divNum.value)
    for (let i=0; i<num; i++) {
        createDiv(div, num, i)
    }
}
function createDiv(parent,num, i) {
    let div = document.createElement("div");
    div.classList = ["ball"];
    let x = (180+200*Math.cos(2*Math.PI*i/num));
    let y = (180+200*Math.sin(2*Math.PI*i/num));
    let color = `rgb(${Math.round(Math.random()*255)}, ${Math.round(Math.random()*255)}, ${Math.round(Math.random()*255)}`;
    div.style.backgroundColor = color;
    div.style.top = `${y}px`;
    div.style.left = `${x}px`;
    parent.append(div)
}

function animateCSS (flag=false) {
    let divContainer = document.getElementById("div-container");
    divContainer.style.animation = flag?`rotate ${10/divSpeed.value}s linear infinite`:"";
}
function animateSetInterval () {
    let collectionDiv = document.getElementsByClassName("ball");
    let count = 1;
    if (Boolean(intervalID)) {
        null;
    } else {
        intervalID = setInterval (
            () => {
                for (let i=0; i<collectionDiv.length; i++) {
                    rotateDiv(collectionDiv[i], collectionDiv.length, i, count)
                    
                }
                count++
            }, 60/Number(divSpeed.value)
        )
    }
}

function rotateDiv(div, num, i, count) {
    let x = (180+200*Math.cos(2*Math.PI*i/num + 0.03*count));
    let y = (180+200*Math.sin(2*Math.PI*i/num + 0.03*count));
    div.style.top = `${y}px`;
    div.style.left = `${x}px`;
}