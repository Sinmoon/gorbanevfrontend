let arr = document.getElementById("array"),
    btn = document.getElementById("btn"),
    massive= [],
    bufArray =[],
    maxArray=[],
    buf;

btn.addEventListener( "click", function () {
    buf=1;
    jsConsole.clearConsole();
    massive = arr.value.split(",");
    bufArray = [massive[0]];
    maxArray = [massive[0]];
    jsConsole.writeLine("The input sequence is:");
    jsConsole.writeLine(massive.join(", "));
    for (let i = 0; i<massive.length - 1; i++) {
        if (Number(massive[i]) < Number(massive[i+1])) {
            buf++
            bufArray.push(massive[i+1]);
            if (bufArray.length > maxArray.length) {
                maxArray = bufArray.slice(0);
            }
        } else {
            buf = 1;
            bufArray = [massive[i+1]];
        }
    }
    jsConsole.writeLine("");
    jsConsole.writeLine("The maximum increasing sequence is:")
    jsConsole.writeLine(maxArray.join(", "));
});




