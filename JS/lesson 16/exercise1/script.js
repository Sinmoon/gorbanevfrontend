let value = document.getElementById("number"),
    btn = document.getElementById("btn"),
    btnResult = document.getElementById("btnResult");

let arr = generateNum();
let bob = checkNumber(arr);
let storage = window.localStorage;

btn.addEventListener("click", ()=> {bob(value.value)});
btnResult.addEventListener("click", showLeaderboard)

function checkNumber (answer) {
    let sheepCount = 0,
         ramCount = 0,
         count = 0,
        answ = answer;

    function check (numArray) {
        ramCount = 0;
        sheepCount = 0;
        count++;
        if (Number(numArray) && numArray.length === 4 && numArray[0] != 0) {
            for (let i = 0; i<answ.length; i++) {
                if (numArray[i] == answ[i]) {
                    ramCount++;
                } else if (answ.some((item) => numArray[i] == item)) {
                    sheepCount++;
                }
            }
            if (ramCount === 4) {
                console.log(`You win for ${count} times`);
                winWindow(count)
            } else {
                let mess = document.createElement("div");
                let div = document.getElementById("container")
                mess.textContent = `You have: ${ramCount} ram(s) and ${sheepCount} sheep(s)`;
                div.append(mess);
            }
        } else {
            let mess = document.createElement("div");
            let div = document.getElementById("container")
            mess.textContent = `Invalid entered number. Number must contain four different digits and can not start with 0.`;
            div.append(mess)
        }
    }
    return check
}

function generateNum(){
    let max = 10;
    let arr = [];
    for(let i = 0; i<4 ; i++){
        let num = Math.floor(Math.random()*max);
        if(arr.indexOf(num) == -1 ){
            arr.push(num);
        }
        else {
         i--;
        }
        if (arr[0] === 0) {
            arr.splice(0,1);
            i--;
        }
    }
    let div = document.getElementById("trueNum");
    div.textContent = `Number: ${arr.join("")}`;
    return arr
}

function winWindow (num) {
    btn.disabled = true;
    btnResult.disabled = true;

    let h = document.createElement("h3");
    h.textContent="You win !";
    
    let winWindow = document.createElement("div");
    winWindow.id = "winWindow";


    let div = document.createElement("div")
    div.textContent = "Nickname: ";

    let input = document.createElement("input");
    input.id = "winner";
    input.placeholder = "Enter your nickname";

    let btnSave = document.createElement("button");
    btnSave.textContent = "Save";
    btnSave.addEventListener("click", () => save(num))

    let body = document.getElementsByTagName("body")[0];
    winWindow.prepend(h);
    div.append(input);
    div.append(btnSave);
    winWindow.append(div)

    body.append(winWindow);

}

function save (num) {
    btn.disabled = false;
    btnResult.disabled = false

    let winner = document.getElementById("winner");
    let winWindow = document.getElementById("winWindow");
    storage.setItem(winner.value, num);

    winWindow.remove();

    arr = generateNum();
    bob = checkNumber(arr);
    value.value = "";
    
    let container = document.getElementById("container");
    for (let i = 3; i< container.children.length;) {
        container.children[i].remove()
    }

}

function showLeaderboard () {
    btn.disabled = true;
    btnResult.disabled = true;

    let container  = document.createElement("div");
    let body = document.getElementsByTagName("body")[0];

    let btnClose = document.createElement("button");
    btnClose.textContent ="X";
    btnClose.addEventListener("click", ()=>{
        container.remove();
        btn.disabled = false;
        btnResult.disabled = false;
    });

    container.id = "leaderboard";
    container.innerHTML = "<div>Nickname:</div><div>Score:</div>";

    for (let i = 0; i<storage.length; i++) {

    }
    for (key in storage) {
        if (!localStorage.hasOwnProperty(key)) {
            continue;
        }
        createTable(key, container);
    }
    container.append(btnClose)

    body.append(container)
}

function createTable (key, container) {
    let value = document.createElement("div");
    let score = document.createElement("div");
    value.textContent = key;
    score.textContent = storage[key]
    container.append(value);
    container.append(score);
}