let val1 = document.getElementById("val1"),
    val2 = document.getElementById('val2'),
    btn = document.getElementById("btn");

btn.addEventListener( "click", startFunc);

function startFunc () {
    let a = val1.value,
        b = val2.value;
    conc(a, b)
}

let conc =  function (a, b) {
    jsConsole.writeLine(`a = ${a}, b = ${b};`);
    a === b ?
        jsConsole.writeLine(`a = b => 1`):
        jsConsole.writeLine(`a != b => -1;`);
        jsConsole.writeLine("")
}

jsConsole.writeLine("Test start #1:");
conc("abc", "abc");
jsConsole.writeLine("Test start #2:");
conc("abC","abc");

