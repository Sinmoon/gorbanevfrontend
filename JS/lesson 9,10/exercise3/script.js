let btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear");

//  __________BUTTONS____________
btn.addEventListener("click", funcStart);
clrBtn.addEventListener("click", function () {jsConsole.clearConsole()});

function funcStart () {
    let man = new Person ("Ivan", "Petrov", 21, "Alex", "Olga");
    let clone = {}
    for (key in man) {
        clone[key] = man[key];
    }
    jsConsole.writeLine(`Object: man`)
    writeInfo(man);
    man.sayHi()
    jsConsole.writeLine(`______________________________________`);

    //  Clone
    jsConsole.writeLine(`Object: clone`)
    writeInfo(clone);
    clone.sayHi()
    jsConsole.writeLine(`______________________________________`);

    //  Reduct clone
    clone.name = "Olga";
    clone.lastname = "Borov";
    clone.age = 41;
    clone.childrens.child1= 'Petya';
    delete clone.childrens.child2;
    clone.sayHi = function () {
        jsConsole.writeLine(`Hi, you need something?`)
    }
    
    jsConsole.writeLine(`Object: clone (reduct )`)
    writeInfo(clone);
    clone.sayHi()
    jsConsole.writeLine(`______________________________________`);
}




function Person (name, lastname, age, child1, child2) {
    this.name = name;
    this.lastname = lastname;
    this.age = Number(age);
    this.childrens = {
        child1: child1,
        child2: child2
    }
}

Person.prototype.sayHi = function () {
    jsConsole.writeLine(`Hello, my name is  ${this.name}`);
}

function writeInfo (obj) {
    for (key in obj) {
        if ( typeof obj[key] == "object") {
            jsConsole.writeLine(`&nbsp&nbsp${key}:`)
            let objkey = key;
            for (key in obj[objkey]) {
                jsConsole.writeLine(`&nbsp&nbsp&nbsp&nbsp${key}: ${obj[objkey][key]}`);
            }
        } else {
            jsConsole.writeLine(`&nbsp&nbsp${key}: ${obj[key]}`);
        }
    }
}
