let arr = document.getElementById("array"),
    btn = document.getElementById("btn"),
    massive= [],
    buf, maxBuf, elem, elemArray;

btn.addEventListener( "click", function () {
    buf=1;
    maxBuf=1;
    jsConsole.clearConsole();
    massive = arr.value.split(",");
    elem = massive[0];
    jsConsole.writeLine("The input sequence is:");
    jsConsole.writeLine(massive.join(", "));
    for (let i = massive.length - 1; i > 0; i--) {
        for (let j = 0; j < i; j++) {
            if (Number(massive[j]) > Number(massive[j + 1])) {
                let buf = massive[j];
                massive[j] = massive[j + 1];
                massive[j + 1] = buf;
            }
        }
    }
    for (let i = 0; i < massive.length-1; i++) {
        if (massive[i] === massive[i+1]) {
            buf++
            if (buf == maxBuf) {
                elemArray.push(massive[i])
            }
            if (buf>maxBuf) {
                maxBuf = buf;
                elem = massive[i+1]
                elemArray = [massive[i+1]];
            }
        } else {
            buf = 1;
        }
    }
    jsConsole.writeLine("");
    jsConsole.writeLine("The most frequent number(s) is:")
    jsConsole.writeLine(`${elemArray.join(", ")} (${maxBuf} times)`);

});




