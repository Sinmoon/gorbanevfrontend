let text = document.getElementById("text"),
    btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear"),
    exmplBtn = document.getElementById("example");

clrBtn.addEventListener("click", () => jsConsole.clearConsole());

exmplBtn.addEventListener("click", () => text.value = "http://www.d3bg.org/forum/index.php");

btn.addEventListener("click", () =>createObj(text.value))

function createObj (text) {
    let url = text.match(/(http[s]?):\/\/([^\/]*)(.*)/);
    let obj = {
        "protocol" : url[1],
        "server" : url[2],
        "resource" : url[3]
    }
    for (key in obj) {
        jsConsole.writeLine(`${key}: ${obj[key]}`)
    }
    jsConsole.writeLine("_________________________")
}