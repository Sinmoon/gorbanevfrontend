let str = document.getElementById("array"),
    index = document.getElementById("index"),
    btn = document.getElementById("btn");

function findNum(str, ind) {
    let array = str.value.split(",");
    if (!ind.value) {
        jsConsole.writeLine(`Enter some number`);
    } else {
        let num = Number(ind.value);
        if (num<0 || num>array.length-1) {
            jsConsole.writeLine(`Enter correct number!`);
        } else {
            num == 0? 
            jsConsole.writeLine(`${array[num]>array[num+1]}`)
            : num == array.length-1?
                jsConsole.writeLine(`${array[num]>array[num-1]}`)
                : jsConsole.writeLine(`${array[num]>array[num+1] && array[num]>array[num-1]} `);
        }
    }
}
function startFunc () {
    findNum(str, index);
}

btn.addEventListener( "click", startFunc);