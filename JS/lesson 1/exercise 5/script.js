let n = document.getElementById("n"),
    nFact = document.getElementById("nFact"),
    resBtn = document.getElementById("res");

    resBtn.addEventListener("click", function () {
        if( n.value) {
            let nVal = Number(n.value);
            let res= nVal;
            do {
                --nVal;
                res = res*nVal;
            } while (nVal !== 1)
            nFact.value = res;
        } else {
            nFact.value = "Enter N";
        }
    })