let text = document.getElementById("text"),
    btn = document.getElementById("btn"),
    clrBtn = document.getElementById("clear"),
    exmplBtn = document.getElementById("example");

clrBtn.addEventListener("click", () => jsConsole.clearConsole());
exmplBtn.addEventListener("click", () => text.value = "<html><head><title>Sample site</title></head><body><div>text<div>more text</div>and more...</div>in body</body></html>");
btn.addEventListener("click", () =>htmlValue(text.value))

function htmlValue (text) {
    jsConsole.writeLine(text.replace(/(\<[^\>]*\>)/g, ''))
}