let num = document.getElementById('num'),
    massive = document.getElementById("massive"),
    max = document.getElementById("max"),
    min = document.getElementById("min"),
    sum = document.getElementById("sum"),
    avarage = document.getElementById("avarage"),
    odd = document.getElementById("odd"),
    res = document.getElementById("res")

res.addEventListener("click", function () {
    odd.value="";
    let numValue = num.value,
        arr = [];
    for (let i = 0; i<numValue; i++) {
        arr.push(Math.round(Math.random()*300));
    }
    massive.value = arr;
    max.value = Math.max(...arr);
    min.value = Math.min(...arr);
    sum.value = arr.reduce((acc, item) => {
        acc+= item;
        return acc;
    },0)
    avarage.value = Math.round(sum.value/arr.length*100)/100;
    arr.forEach((item) => {
        item % 2 !==0? odd.value += `${item},`:null;
    })
    odd.value = odd.value.slice(0, odd.value.length-1)
    odd.value===""?odd.value="none":null
})